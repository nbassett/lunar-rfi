# \*\*\*THIS REPOSITORY HAS BEEN MOVED TO GITHUB\*\*\*
Please use the repository at [https://github.com/npbassett/lunar-RFI](https://github.com/npbassett/lunar-RFI) rather than this Bitbucket repository.

# Lunar Low Frequency Environment #

This repository contains scripts for characterizing the low frequency radio environment behind the moon. The `FDTD_simulations` folder contains scripts to run electrodynamics simulations of the diffraction of radio waves around the Moon. The `calc_width` folder uses the results of these simulations to claculate the width of the quiet region given the frequency, height above the surface of the Moon, and intensity threshold. Special thanks to the [Meep](https://meep.readthedocs.io/en/latest/) software team for providing an open-source platform for the simulations performed in this work.

### Contact ###

* Neil.Bassett@colorado.edu
* [nbassett.bitbucket.io](https://nbassett.bitbucket.io/)